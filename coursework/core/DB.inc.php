<?php
class DB
{
    protected $pdo;
    public function __construct($host,$user,$pass,$dbname)
    {
        $this->pdo = new PDO("mysql:host={$host};dbname={$dbname}",$user,$pass);
    }

    public function AddNewUser($row)
    {
        $stmt = $this->pdo->prepare('insert into users(login,password,email,token) values(:login,:password,:email,:token)');
        $stmt->bindParam(':login',$row['login']);
        $passhash = md5($row['password']);
        $stmt->bindParam(':password',$passhash);
        $stmt->bindParam(':email',$row['email']);
        $stmt->bindParam(':token',$row['token']);
        $stmt->execute();
    }

    public function IsLoginUsed($login)
    {
        $stmt = $this->pdo->prepare('select count(*) from users where login=:login');
        $stmt->bindParam(':login',$login);
        $stmt->execute();

        $res = $stmt->fetch();
        return $res[0];
    }

    public function IsEmailUsed($email)
    {
        $stmt = $this->pdo->prepare('select count(*) from users where email=:email');
        $stmt->bindParam(':email',$email);
        $stmt->execute();

        $res = $stmt->fetch();

        return $res[0];
    }

    public function getUserPasswordByLogin($login)
    {
        $stmt = $this->pdo->prepare('select login,role,password,status from users where login=:login');
        $stmt->bindParam(':login',$login);
        $stmt->execute();
        $res = $stmt->fetchAll();
        if(count($res)==1 && $res[0]['status']=1)
            return $res[0];
        else
            return -1;
    }

    public function AddFilm($arr)
    {
        $stmt = $this->pdo->prepare("INSERT INTO films (name, originalName,
                                                ageRestriction, director, scenario,duration,
                                                production, starring, genre,
                                                 language, imageId, filmDescription) 
                                                VALUES ( :name, :originalName,
                                                 :ageRestriction, :director, :scenario, :duration,
                                                  :production, :starring, :genre, :language,
                                                  :imageId, :filmDescription)");
        $stmt->bindParam(':name',$arr['name']);
        $stmt->bindParam(':originalName',$arr['originalName']);
        $stmt->bindParam(':ageRestriction',$arr['ageRestriction']);
        $stmt->bindParam(':scenario',$arr['scenario']);
        $stmt->bindParam(':duration',$arr['duration']);
        $stmt->bindParam(':production',$arr['production']);
        $stmt->bindParam(':starring',$arr['starring']);
        $stmt->bindParam(':genre',$arr['genre']);
        $stmt->bindParam(':language',$arr['language']);
        $stmt->bindParam(':imageId',$arr['imageId']);
        $stmt->bindParam(':filmDescription',$arr['filmDescription']);
        $stmt->bindParam(':director',$arr['director']);

        $res = $stmt->execute();

        return $res;

    }

    public function GetFilmById($id)
    {
        $stmt = $this->pdo->prepare('select * from films where id=:id');
        $stmt->bindParam(':id',$id);
        $stmt->execute();
        $res = $stmt->fetchAll();
        return $res;
    }

    public function GetCurrentMoviesPreview()
    {
        // select active movies instead of all that is selcted for test
        $stmt = $this->pdo->prepare('select films.id,films.name,films.filmDescription,films.imageId from films
INNER JOIN session on session.movieId = films.id
where session.date >= :currentdate
group by films.id,films.name,films.filmDescription,films.imageId');
        /*
(select session.date from films
INNER JOIN session on session.movieId = films.id
where session.date>=:currentdate
group by films.id,films.name,films.filmDescription,films.imageId
ORDER BY session.date
limit 1)*/
        // $stmt = $this->pdo->prepare('select id,name from films ');
        $date = date("Y-m-d");

        $stmt->bindParam(':currentdate',$date);
        ///:currentdate
        $stmt->execute();
        $res = $stmt->fetchAll();
        return $res;
    }

    public function GetSessionsByMovieId($id)
    {
        // select active movies instead of all that is selcted for test
        // $stmt = $this->pdo->prepare('select id,name from films ');
        $stmt = $this->pdo->prepare('select session.date,session.time,session.id from films
INNER JOIN session on session.movieId = films.id
where session.date>=:currentdate and films.id = :id
ORDER BY session.date');
        $date = date("Y-m-d");

        $stmt->bindParam(':currentdate',$date);
        $stmt->bindParam(':id',$id);
        ///:currentdate
        $stmt->execute();
        $res = $stmt->fetchAll();
        return $res;
    }
/*
 select session.date,session.time from films
INNER JOIN session on session.movieId = films.id
where session.date =
(select session.date from films
INNER JOIN session on session.movieId = films.id
where session.date>=:currentdate and films.id = :id
ORDER BY session.date
limit 1)');
 */
    //GetAllFilms

    public function GetAllFilms()
    {
        // select active movies instead of all that is selcted for test
        $stmt = $this->pdo->prepare('select id,name from films ');
        $stmt->execute();
        $res = $stmt->fetchAll();
        return $res;
    }

    public function AddSession($arr)
    {
        $stmt = $this->pdo->prepare('insert into session(movieId,date,time) values(:movieId,:date,:time)');
        $stmt->bindParam(':movieId',$arr['id']);
        $stmt->bindParam(':date',$arr['date']);
        $stmt->bindParam(':time',$arr['time']);
        $res = $stmt->execute();
        return $res;
    }

    public function GetFilmInfoBySessionId($sessionId)
    {
        $stmt = $this->pdo->prepare('select name,date,time from films inner join session on films.id = session.movieid where session.id = :sessid');
        $stmt->bindParam(':sessid',$sessionId);
        $stmt->execute();
        $res = $stmt->fetchAll();
        return $res;
    }

    public function GetTicketsBySessionId($sessionId)
    {
        $stmt = $this->pdo->prepare('select row,seat,price,issold from session  inner join seats on seats.sessionId = session.id where session.id = :sessid');
        $stmt->bindParam(':sessid',$sessionId);
        $stmt->execute();
        $res = $stmt->fetchAll();
        return $res;
    }

    public function OrderSeats($seatsObjArr,$sessionId)
    {
        $stmt = $this->pdo->prepare('update seats set isSold = 1 where seats.sessionid = :sessid and row = :row and seat = :seat');
        foreach ($seatsObjArr as $item) {
            $stmt->bindParam(':sessid',$sessionId);
            $stmt->bindParam(':row',$item->row);
            $stmt->bindParam(':seat',$item->seat);

            $stmt->execute();
        }

        return 1;
    }

    public function CheckIfSeatsIsNotSold($seatsObjArr,$sessionId)
    {
        $stmt = $this->pdo->prepare('select count(*) from seats where seats.sessionid = :sessid and row = :row and seat = :seat and issold=1');
        foreach ($seatsObjArr as $item) {
            $stmt->bindParam(':sessid',$sessionId);
            $stmt->bindParam(':row',$item->row);
            $stmt->bindParam(':seat',$item->seat);
            $stmt->execute();
            $res = $stmt->fetchAll();
            if($res[0][0]==1)
                return -1;
        }

        return 1;
    }

    public function GetComments($id,$limit,$offset)
    {//select * from comments inner join films on films.id = comments.filmid where filmid = 0 limit 1 OFFSET 1
        $stmt = $this->pdo->prepare('select comments.id,comments.userid,comments.content,DATE_FORMAT(comments.datetime, \'%d.%m.%Y %H:%i:%s\') as datetime,users.login,users.imageid from comments 
                               inner join films on films.id = comments.filmid
                               inner join users on users.id = comments.userid
                               where filmid=:filmid order by DATE_FORMAT(comments.datetime, \'%d.%m.%Y %H:%i:%s\') desc  limit :limit offset :offset ');
        $id = intval($id);
        $limit = intval($limit);
        $offset = intval($offset);
        $stmt->bindParam(':filmid',$id);
        $stmt->bindParam(':limit',$limit,PDO::PARAM_INT);
        $stmt->bindParam(':offset',$offset,PDO::PARAM_INT);
        $stmt->execute();
        $res = $stmt->fetchAll();

        return $res;
    }

    public function AddComment($filmid, $userlogin, $datetime, $content)
    {
        $stmt = $this->pdo->prepare('select id from users where login=:login');
        $stmt->bindParam(':login',$userlogin);
        $stmt->execute();
        $res = $stmt->fetchAll()[0]['id'];

        $stmt = $this->pdo->prepare('insert into comments(userid,filmid,content,datetime) values(:userid,:filmid,:content,:datetime)');
        $stmt->bindParam(':userid',$res);
        $stmt->bindParam(':filmid',$filmid);
        $stmt->bindParam(':content',$content);
        $stmt->bindParam(':datetime',$datetime);
        $stmt->execute();
        $res = $stmt->fetchAll();
        return $res;
    }

    public function GetRating($filmid,$userlogin)
    {
        $stmt = $this->pdo->prepare('select avg(mark) as rating from marks where filmid=:filmid');
        $stmt->bindParam(':filmid',$filmid);
        $stmt->execute();
        $res = [];
        $res['rating'] = $stmt->fetchAll()[0];

        if($userlogin!=null && $userlogin!=''){
            $stmt = $this->pdo->prepare('select id from users where login=:login');
            $stmt->bindParam(':login',$userlogin);
            $stmt->execute();
            $userid = $stmt->fetchAll()[0]['id'];

            if($userid!=null && $userid!=0)
            {
                $stmt = $this->pdo->prepare('select mark from marks where filmid=:filmid and userid=:userid');
                $stmt->bindParam(':filmid',$filmid);
                $stmt->bindParam(':userid',$userid);
                $stmt->execute();
                $m =  $stmt->fetchAll();
                if(count($m)>0)
                    $res['userRating'] = $m[0];
            }
        }
        return $res;
    }

    public function setRating($filmid, $userlogin, $rating)
    {
        $stmt = $this->pdo->prepare('select id from users where login=:login');
        $stmt->bindParam(':login', $userlogin);
        $stmt->execute();
        $userid = $stmt->fetchAll()[0]['id'];

        $stmt = $this->pdo->prepare('select count(*) from marks where filmid=:filmid and userid=:userid');
        $stmt->bindParam(':filmid', $filmid);
        $stmt->bindParam(':userid', $userid);
        $stmt->execute();
        $res = $stmt->fetchAll();

        if ($res[0][0] == 0){

            $stmt = $this->pdo->prepare('insert into marks(filmid,userid,mark) values(:filmid,:userid,:mark)');
            $stmt->bindParam(':filmid',$filmid);
            $stmt->bindParam(':userid',$userid);
            $stmt->bindParam(':mark',$rating);
            $stmt->execute();
        }
        else{

            $stmt = $this->pdo->prepare('update marks set mark=:mark where filmid=:filmid and userid=:userid');
            $stmt->bindParam(':filmid',$filmid);
            $stmt->bindParam(':userid',$userid);
            $stmt->bindParam(':mark',$rating);
            $stmt->execute();

        }

        return $stmt->errorCode();
    }

    public function ActivateAccount($token)
    {//
        $stmt = $this->pdo->prepare('update users set status=1 where token=:token ');
        $stmt->bindParam(':token',$token);
        $stmt->execute();

    }

    public function ResetPassword($email,$password)
    {
        $stmt = $this->pdo->prepare('update users set password=:password where email=:email ');
        $stmt->bindParam(':email',$email);
        $stmt->bindParam(':password',md5($password));
        $stmt->execute();
    }

    public function GetUserIdByLogin($login)
    {
        $stmt = $this->pdo->prepare('select id from users where login=:login');
        $stmt->bindParam(':login', $login);
        $stmt->execute();
        $userid = $stmt->fetchAll()[0]['id'];
        return $userid;
    }

    public function GetUsersFilms($login)
    {
        $userid = $this->GetUserIdByLogin($login);

        $stmt = $this->pdo->prepare('select films.id,films.name,session.date,session.time,seats.price,seats.row,seats.seat from films
                                                inner join session on session.movieId = films.id
                                                inner join seats on seats.sessionId = session.id
                                                inner join sold on sold.seatid = seats.id
                                                inner join users on users.id = sold.userid
                                                where users.id =:userid');
        $stmt->bindParam(':userid', $userid);
        $stmt->execute();
        $res = $stmt->fetchAll();
        return $res;
    }

    public function AddToHistory($seats,$login,$sessionId)
    {
        $userid = $this->GetUserIdByLogin($login);
        foreach ($seats as $item)
        {
            $item = (array)$item;
            $stmt = $this->pdo->prepare('select id from seats where seat=:seat and row=:row and sessionid=:sessionid');
            $stmt->bindParam(':seat',$item['seat']);
            $stmt->bindParam(':row',$item['row']);
            $stmt->bindParam(':sessionid',$sessionId);
            $stmt->execute();
            $seatid = $stmt->fetchAll()[0][0];

            $stmt = $this->pdo->prepare('insert into sold(userid,seatid) values(:userid,:seatid)');
            $stmt->bindParam(':userid',$userid);
            $stmt->bindParam(':seatid',$seatid);
            $stmt->execute();
        }
    }

    public function GetUserImageId($login)
    {
        $userid = $this->GetUserIdByLogin($login);

        $stmt = $this->pdo->prepare('select imageId from users where id=:id');
        $stmt->bindParam(':id',$userid);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function UpdateAvatar($login,$imageId)
    {
        $stmt = $this->pdo->prepare('update users set imageid=:imageid where login=:login');
        $stmt->bindParam(':imageid',$imageId);
        $stmt->bindParam(':login',$login);
        $stmt->execute();
    }

    public function UpdatePassword($login,$old,$new)
    {
        $old = md5($old);
        $new = md5($new);
        $stmt = $this->pdo->prepare('update users set password=:new where login=:login and password=:old');
        $stmt->bindParam(':login',$login);
        $stmt->bindParam(':old',$old);
        $stmt->bindParam(':new',$new);
        $stmt->execute();
        $res = $stmt->rowCount();

        return $res;
    }

    public function deleteCommnet($id)
    {
        $stmt = $this->pdo->prepare('delete from comments where id=:id');
        $stmt->bindParam(':id',$id);
        $res = $stmt->execute();
        return $res;
    }

    public function DeleteSessionById($id)
    {
        $stmt = $this->pdo->prepare('delete from session where id=:id');
        $stmt->bindParam(':id',$id);
        $res = $stmt->execute();
        return $stmt->errorInfo();
    }

    public function GetFilmFullInfo($id)
    {
        $stmt = $this->pdo->prepare('select * from films where id=:id');
        $stmt->bindParam(':id',$id);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function UpdateFilm($row)
    {
        $stmt = $this->pdo->prepare('update films set name=:name,ageRestriction=:ageRestriction ,originalName=:originalName ,director=:director
                                                ,language=:language ,genre=:genre ,duration=:duration,production=:production,
                                                scenario=:scenario,starring=:starring,filmDescription=:filmDescription,imageId=:imageId where id=:id');
        $stmt->bindParam(':id',$row['id']);

        $stmt->bindParam(':name',$row['name']);
        $stmt->bindParam(':ageRestriction',$row['ageRestriction']);
        $stmt->bindParam(':originalName',$row['originalName']);
        $stmt->bindParam(':director',$row['director']);
        $stmt->bindParam(':language',$row['language']);
        $stmt->bindParam(':genre',$row['genre']);
        $stmt->bindParam(':duration',$row['duration']);
        $stmt->bindParam(':production',$row['production']);
        $stmt->bindParam(':scenario',$row['scenario']);
        $stmt->bindParam(':starring',$row['starring']);
        $stmt->bindParam(':filmDescription',$row['filmDescription']);
        $stmt->bindParam(':imageId',$row['imageId']);
        $stmt->execute();
    }

    public function DeleteFilm($id)
    {
        $stmt = $this->pdo->prepare('delete from films where id=:id');
        $stmt->bindParam(':id',$id);
        $stmt->execute();
        return $stmt->errorInfo();
    }
}
