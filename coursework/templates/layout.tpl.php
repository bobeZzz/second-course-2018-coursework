<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Cinema</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="/templates/css/style.css" rel="stylesheet" type="text/css">

</head>

<body>
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="/">Cinema</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <?php
                //if($_SESSION['role']=='admin' || $_SESSION['role']=='moderator')
                    // display controls
                ?>
                <?php
                if(!isset($_SESSION['login']))
                    echo '<li class="nav-item">
                    <a class="nav-link" href="/users/register">Register</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/users/login">Login</a>
                </li>';
                else{
                    if($_SESSION['role']=='admin' || $_SESSION['role']=='moderator')
                    {
                        echo '<li class="nav-item">
                        <a class="nav-link" href="/films/editFilm">Edit film</a>
                    </li>   ';
                        echo '<li class="nav-item">
                        <a class="nav-link" href="/films/add">Add film</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/films/addSession">Add session</a>
                    </li>';

                        }
                    echo '<div class="dropdown">
                      
                      <a class="nav-link dropdown-toggle"  data-toggle="dropdown">
                      <i class="fa fa-user-circle " aria-hidden="true"></i>
                      <span class="caret"></span></a>
                      <ul class="dropdown-menu bg-dark">
                        <li><a class="nav-link" href="#" id="user-login">'.$_SESSION['login'].'</a></li>
                        <li class="divider"></li>
                        <li><a class="nav-link" href="/users/profile">Profile</a></li>
                                                <li class="divider"></li>

                        <li><a class="nav-link" href="/users/logout">Logout</a></li>
                      </ul>
                    </div>';
                }
                /*'<li class="nav-item">
                    <a class="nav-link" href="/users/myaccount"><i class="fa fa-user-circle" aria-hidden="true"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/users/logout">Logout</a>
                </li>'*/
                ?>

            </ul>
        </div>
    </div>
</nav>

<?php include($content_view)?>

<!-- Footer -->
<footer class="py-5 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Your Website 2018</p>
    </div>
    <!-- /.container -->
</footer>

</body>

</html>
