<!-- Page Content -->
<div class="container">

    <div class="row">
        <div class="col-lg-2    ">
        </div>
        <!-- Post Content Column -->
        <div class="col-lg-8">

            <!-- Title -->
            <h2 class="mt-4">Додати сеанс</h2>


            <!-- Preview Image -->
            <!-- <img class="img-fluid rounded film-image" src="/files/films/filmID.jpeg" alt=""> -->
            <form action="/films/addSession" method="post" enctype="multipart/form-data">
                <hr>
                <ul class="list-group add-form">
                    <li class="list-group-item">
                        <p class="key">Назва:</p>
                            <br>
                            <select name="id" class="full-width">
                                <?php
                                foreach ($films as $film)
                                    //echo $film['id'].'----'.$film['name'].'<br>';
                                    echo '<option value="'.$film['id'].'">'.$film['name'].'</option>'
                                ?>
                            </select>
                        </p>
                    </li>
                    <li class="list-group-item">
                        <p class="key">Дата:</p>
                        <p class="val ">
                            <input type="date" name="date" class="date-time" value="<?= date("Y-m-d");?>">
                        </p>
                    </li>
                    <li class="list-group-item">
                        <p class="key">Час:</p>
                        <p class="val ">

                            <input type="time" name="time" class="date-time" value="<?= date("H:i");?>">
                        </p>
                    </li>

                </ul>
                <!-- Post Content -->
                <button type="submit" class="btn btn-dark btn-lg btn-block login-button" id="sub">Додати</button>
                <br>
            </form>
            <script src="/templates/modules/films/addNewMovie.js"></script>
        </div>



        </div>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->


<script src="/templates/modules/films/selectHint.js"></script>