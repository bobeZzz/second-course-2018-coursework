<!-- Page Content -->
<div class="container">

    <div class="row">

        <!-- Post Content Column -->
        <div class="col-xl-8 ">
            <input type="hidden" value="<?=$info['sessionId']?>" id="sessionId">
            <form action="/films/buyTickets" method="post" enctype="multipart/form-data">

            <!-- Title -->
                <h2 class="mt-4">Оберіть місця для сеансу:</h2>
                <h3 class="mt-4"><?=$info['name']?> -> <?=$info['date']?> -> <?=$info['time']?></h3>

            <!-- Preview Image -->
            <!-- <img class="img-fluid rounded film-image" src="/files/films/filmID.jpeg" alt=""> -->

                <hr>

                <div class="hall">
                    <div class="hall-screen">SCREEN</div>
                    <?php
                    foreach ($seats as $key=>$row){
                        //var_dump(get_defined_vars());
                        //die();
                        if($key!=count($seats)-1)
                            echo '<div class="row hall-row">';
                        else
                            echo '<div class="row hall-row lux">';
                        foreach ($row as $item) {
                            extract($item);
                            include "hallRowSeat.tpl.php";
                        }
                        echo '</div>';

                    }
                    ?>
                    <!--
                    <div class="row hall-row">
                        <div class="seat sold"><span>1</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>2</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>3</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>4</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>5</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>6</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>7</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>8</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>9</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>10</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>11</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>12</span><span class="fa fa-user" style="display: none"></span></div>
                    </div>
                    <div class="row hall-row">
                        <div class="seat"><span>1</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>2</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>3</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>4</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>5</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>6</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>7</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>8</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>9</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>10</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>11</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>12</span><span class="fa fa-user" style="display: none"></span></div>
                    </div>
                    <div class="row hall-row">
                        <div class="seat"><span>1</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>2</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>3</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>4</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>5</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>6</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>7</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>8</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>9</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>10</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>11</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>12</span><span class="fa fa-user" style="display: none"></span></div>
                    </div>
                    <div class="row hall-row">
                        <div class="seat"><span>1</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>2</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>3</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>4</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>5</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>6</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>7</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>8</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>9</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>10</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>11</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>12</span><span class="fa fa-user" style="display: none"></span></div>
                    </div>
                    <div class="row hall-row">
                        <div class="seat"><span>1</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>2</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>3</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>4</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>5</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>6</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>7</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>8</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>9</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>10</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>11</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>12</span><span class="fa fa-user" style="display: none"></span></div>
                    </div>
                    <div class="row hall-row">
                        <div class="seat"><span>1</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>2</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>3</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>4</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>5</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>6</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>7</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>8</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>9</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>10</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>11</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>12</span><span class="fa fa-user" style="display: none"></span></div>
                    </div>
                    <div class="row hall-row lux">
                        <div class="seat"><span>1</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>2</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>3</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>4</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>5</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>6</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>7</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>8</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>9</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>10</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>11</span><span class="fa fa-user" style="display: none"></span></div>
                        <div class="seat"><span>12</span><span class="fa fa-user" style="display: none"></span></div>
                    </div>
                    -->
                </div>
                <hr>


                <!-- Post Content -->

            </form>
        </div>

        <div class="col-xl-4 col-md-12">

            <!-- Search Widget -->
            <div class="card my-4">
                <h5 class="card-header">
                    Обрані місця:
                </h5>
                <div class="card-body">
                    <button type="submit" class="btn btn-dark btn-md btn-block login-button" id="sub" disabled>Забронювати</button>
                    <div id="selected-seats">
                        <div id="ticket-list">
                            <span>Ряд</span>
                            <span>Місце</span>
                            <span>Ціна (грн)</span>
                        </div>
                    </div>
                    <div id="result-info">
                        <div class='successNotif' style="display: none"><span>Білети успішно зарезервовані.</span></div>
                        <div class='errorNotif' style="display: none"><span>Щось пішло не так. Спробуйте ще раз пізніше.</span></div>
                    </div>
                </div>
            </div>


        </div>


    </div>
    <!-- /.row -->

</div>
<!-- /.container -->


<script src="/templates/modules/films/ticketSelectionScript.js"></script>
<script src="/templates/modules/films/ticketOrderSubmit.js"></script>