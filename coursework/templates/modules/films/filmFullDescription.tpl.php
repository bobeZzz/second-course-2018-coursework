    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Post Content Column -->
            <div class="col-lg-8">

                <!-- Title -->
                <h2 class="mt-4"><?=$name?></h2>

                <hr>

                <!-- Preview Image -->
                <img class="img-fluid rounded film-image" src=<?="/files/films/".$imageId?> alt="">
                <input type="hidden" value="<?=$id?>" id="filmid">
                <hr>
                <ul class="list-group">
                    <li class="list-group-item"><p class="key">Вік:</p><p class="val"><?=$ageRestriction?>+</p></li>
                    <li class="list-group-item"><p class="key">Оригінальна назва:</p><p class="val"><?=$originalName?></p></li>
                    <li class="list-group-item"><p class="key">Режисер:</p><p class="val"><?=$director?></p></li>
                    <!--<li class="list-group-item"><p class="key">Період проката:</p><p class="val"><?='dobavit chas prokata'?></p></li>-->
                    <li class="list-group-item"><p class="key">Мова:</p><p class="val"><?=$language?></p></li>
                    <li class="list-group-item"><p class="key">Жанр:</p><p class="val"><?=$genre?></p></li>
                    <li class="list-group-item"><p class="key">Тривалість:</p><p class="val"><?=$duration?></p></li>
                    <li class="list-group-item"><p class="key">Виробництво:</p><p class="val"><?=$production?></p></li>
                    <li class="list-group-item"><p class="key">Сценарій:</p><p class="val"><?=$scenario?></p></li>
                    <li class="list-group-item"><p class="key">В головних ролях:</p><p class="val"><?=$starring?></p></li>
                </ul>
                <!-- Post Content -->

                <p class="film-description"><?=$filmDescription?></p>

                <hr>

                <!-- Comments Form -->
                <?php
                if(isset($_SESSION['login']))
                    echo '<div class="card my-4">
                                <h5 class="card-header">Leave a Comment:</h5>
                                <div class="card-body">
                                    <form>
                                        <div class="form-group">
                                            <textarea class="form-control" rows="3"></textarea>
                                        </div>
                                        <button class="btn btn-dark" id="sendComment">Submit</button>
                                    </form>
                                </div>
                            </div>';
                ?>

                <div id="comment-container">

                    <a class="btn btn-dark" id="getMoreComments">Ще коментарі</a>
                </div>

            </div>

            <!-- Sidebar Widgets Column -->
            <div class="col-md-4">

                <!-- Search Widget -->
                <div class="card my-4">
                    <h5 class="card-header">
                        Забронювати білети на:
                    </h5>
                    <div class="card-body">
                        <div class="form-group">
                            <select class="custom-select selectDay">
                                <?php
                                foreach ($sessions as $date => $times)
                                {
                                    echo "<option value='".$date."'>".$date."</option>";

                                }
                                ?>
                            </select>
                            <?php
                            foreach ($sessions as $date => $times) {
                                //var_dump(key($sessions));
                                echo "<div hidden date-times=".$date.">";
                                foreach ($times as $item) {
                                    if(isset($_SESSION['role'])){
                                        if($_SESSION['role']=='admin')
                                        echo "<div data-sessionid='".$item['id']."'><a class='session-time' href=/films/ticketSelection/".$item['id'].">" . $item['time'] . "</a><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></div>";
                                    }
                                    if(isset($_SESSION['login']))
                                        echo "<div data-sessionid='".$item['id']."'><a class='session-time' href=/films/ticketSelection/".$item['id'].">" . $item['time'] . "</a></div>";
                                }
                                echo  "</div>";
                            }
                            ?>

                        </div>
                    </div>
                </div>
                <input type="hidden" id="role" value="<?=$_SESSION['role']?>">
                <!-- Categories Widget -->
                <div class="card my-4">
                    <h5 class="card-header">Rating</h5>
                    <div class="card-body">
                        <?php
                            require "templates/modules/rating/stars.tpl.php"
                        ?>

                    </div>
                </div>



            </div>

        </div>
        <!-- /.row -->

    </div>
    <?php
    if (isset($_SESSION['login']))
        echo '<script src="/templates/modules/rating/ratingController.js"></script>';
    if(isset($_SESSION['role']))
    if ($_SESSION['role']=='admin')
        echo '    <script src="/templates/modules/films/sessionController.js"></script>';
    ?>
    <script src="/templates/modules/films/selectDay.js"></script>
    <script src="/templates/modules/films/commentsLoader.js"></script>
    <!-- /.container -->