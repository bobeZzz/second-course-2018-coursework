
$(".selectDay option:selected").each(function () {

    var date = $(this).text();
    var container = $(this).parent().parent();

    $("div[date-times="+date+"]",container).removeAttr('hidden');
});

$(".selectDay").change(function () {
    var container = $(this).parent().parent();
    $("div[date-times]",container).attr('hidden','');
    var date = $(".selectDay option:selected",container).text();
    $("div[date-times="+date+"]",container).removeAttr('hidden');
});


