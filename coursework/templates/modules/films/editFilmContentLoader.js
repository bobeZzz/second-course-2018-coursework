/*$.ajax({
    url: '/comments/getCommentsToFilm',
    data: {
        id: filmId,
        offset: offset,
        commentsN: commentsN
    },
    type: 'POST',
    success: function (res) {
    }
});*/
var filmid;
window.onload = function () {
    filmid = $("#filmSelect option:selected").attr('data-id');

    $("#filmSelect").change(LoadFilm);
    LoadFilm();
};


function LoadFilm() {
    $.ajax({
        url: '/films/getFilmById',
        data: {
            id: $("#filmSelect option:selected").attr('data-id'),
        },
        type: 'POST',
        success: function (res) {
            var arr = JSON.parse(res);
            $("input[name=id]").val(arr['id']);
            $("input[name=imageId]").val(arr['imageId']);
            $("input[name=name]").val(arr['name']);
            $("input[name=ageRestriction]").val(arr['ageRestriction']);
            $("input[name=originalName]").val(arr['originalName']);
            $("input[name=director]").val(arr['director']);
            $("input[name=language]").val(arr['language']);
            $("input[name=genre]").val(arr['genre']);
            $("input[name=duration]").val(arr['duration']);
            $("input[name=production]").val(arr['production']);
            $("input[name=scenario]").val(arr['scenario']);
            $("input[name=starring]").val(arr['starring']);
            $("textarea[name=filmDescription]").val(arr['filmDescription']);
            $("img.current-img").attr('src','/files/films/'+arr['imageId']);
        }
    })
}