$("#delete").click(function (ev) {
    ev.preventDefault();
    var id = $("option:selected").attr('data-id');

    $("option:selected").remove();

    $("option:first-child").trigger('change');

    $.ajax({
        url: '/films/delete',
        data: {
            id: id
        },
        type: 'POST',
        success: function (res) {
            console.log(res);
        }
    });
});