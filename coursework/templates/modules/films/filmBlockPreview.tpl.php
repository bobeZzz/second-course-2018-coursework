<!-- Project One -->
<div class="row">
    <div class="col-md-5">
        <a href="/films/movie/<?=$id?>">
            <img class="img-fluid rounded mb-3 mb-md-0 poster-preview" src="<?='/files/films/'.$imageId?>" alt="">
        </a>
    </div>
    <div class="col-md-7">
        <h3><a href="/films/movie/<?=$id?>"><?=$name?></a></h3>
        <hr>
        <p><?=$filmDescription?></p>
        <hr>
        <?php require "templates/modules/rating/stars.tpl.php"?>
        <hr>
        <p>Забронювати білети на:</p>
        <div class="form-group">
            <select class="custom-select selectDay" >
            <?php
            foreach ($sessions as $date => $times)
            {
                echo "<option value='".$date."'>".$date."</option>";

            }
            ?>
            </select>
            <?php
            foreach ($sessions as $date => $times) {
                //var_dump(key($sessions));
                echo "<div hidden date-times=".$date.">";
                foreach ($times as $item) {
                    if(isset($_SESSION['role'])) {
                        if ($_SESSION['role'] == 'admin')
                            echo "<div data-sessionid='" . $item['id'] . "'><a class='session-time' href=/films/ticketSelection/" . $item['id'] . ">" . $item['time'] . "</a><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></div>";
                    }
                    if(isset($_SESSION['login']))
                        echo  "<div data-sessionid='".$item['id']."'><a class='session-time' href=/films/ticketSelection/".$item['id'].">" . $item['time'] . "</a></div>";
                };
                echo  "</div>";
            }

            ?>
        </div>
        <hr>
        <a class="btn btn-dark" href="/films/movie/<?=$id?>">Детальніше</a>
    </div>
</div>
<script src="/templates/modules/films/selectDay.js"></script>
<!-- /.row -->
<?php
if(isset($_SESSION['role']))
    if ($_SESSION['role']=='admin')
        echo '    <script src="/templates/modules/films/sessionController.js"></script>';
?>
<hr>