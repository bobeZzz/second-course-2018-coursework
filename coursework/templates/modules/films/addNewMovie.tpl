<!-- Page Content -->
<div class="container">

    <div class="row">

        <!-- Post Content Column -->
        <div class="col-lg-2    ">
        </div>
        <div class="col-lg-8">

            <!-- Title -->
            <h2 class="mt-4">Додати фільм</h2>


            <!-- Preview Image -->
            <!-- <img class="img-fluid rounded film-image" src="/files/films/filmID.jpeg" alt=""> -->
            <form action="/films/add" method="post" enctype="multipart/form-data">
                <hr>
                <ul class="list-group add-form">
                    <li class="list-group-item">
                        <p class="key">Назва:</p>
                        <p class="val ">
                            <input type="text" name="name" class="full-width">
                        </p>
                    </li>
                    <li class="list-group-item">
                        <p class="key">Вік:</p>
                        <p class="val">
                            <input type="number" name="ageRestriction">
                            +
                        </p>
                    </li>
                    <li class="list-group-item">
                        <p class="key">Оригінальна назва:</p>
                        <p class="val">
                            <input type="text" name="originalName" class="full-width">
                        </p>
                    </li>
                    <li class="list-group-item">
                        <p class="key">Режисер:</p>
                        <p class="val">
                            <input type="text" name="director" class="full-width">
                        </p>
                    </li>
                    <li class="list-group-item">
                        <p class="key">Мова:</p>
                        <p class="val">
                            <input type="text" name="language" class="full-width">
                        </p>
                    </li>
                    <li class="list-group-item">
                        <p class="key">Жанр:</p>
                        <p class="val">
                            <input type="text" name="genre" class="full-width">
                        </p>
                    </li>
                    <li class="list-group-item">
                        <p class="key">Тривалість:</p>
                        <p class="val">
                            <input type="time" name="duration">
                        </p>
                    </li>
                    <li class="list-group-item">
                        <p class="key">Виробництво:</p>
                        <p class="val">
                            <input type="text" name="production">
                        </p>
                    </li>
                    <li class="list-group-item">
                        <p class="key">Сценарій:</p>
                        <p class="val">
                            <input type="text" name="scenario" class="full-width">
                        </p>
                    </li>
                    <li class="list-group-item">
                        <p class="key">В головних ролях:</p>
                        <p class="val">
                            <input type="text" name="starring" class="full-width">
                        </p>
                    </li>
                    <li class="list-group-item">
                        <p class="key">Опис фільму:</p>
                        <p class="val">
                            <textarea name="filmDescription" rows="8"></textarea>
                        </p>
                    </li>
                    <li class="list-group-item">
                        <p class="key">Постер:</p>
                        <p class="val">
                            <input type="file" name="poster">
                        </p>
                    </li>
                </ul>
            <!-- Post Content -->
                <button type="submit" class="btn btn-dark btn-lg btn-block login-button" id="sub">Додати</button>
                <br>
            </form>
            <script src="/templates/modules/films/addNewMovie.js"></script>
        </div>




        </div>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->