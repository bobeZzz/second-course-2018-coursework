$('div.seat').click(function () {
    if(this.classList.contains('sold'))
        return;
    $(this).toggleClass('selected-seat');
    $('span:first-child',this).toggle();
    $('span:last-child',this).toggle();
    var seatInd = $('div.seat',$(this).parent()).index(this)+1;
    var rowInd = $('div.row.hall-row').index($(this).parent())+1;

    if(this.classList.contains('selected-seat')){
        var div =$('<div class="choosed" id="'+rowInd+'-'+seatInd+'">' +
            '<span class="row-index">'+(rowInd)+'</span>' +
            '<span class="seat-index">'+(seatInd)+'</span>' +
            '<span class="price">'+$(this).attr("data-price")+'</span>' +
            '</div>');

        $("#selected-seats").append(div);

    }
    else{
        var target = $('div#'+rowInd+'-'+seatInd);
        target.remove();
        //target.fadeOut(300, function(){ $(this).remove();});
    }


    if($("#selected-seats > div.choosed").length>0){
        $("#selected-seats").parent().find('button').removeAttr('disabled');
        $("#ticket-list").show();
    }
    else{
        $("#selected-seats").parent().find('button').prop('disabled', true);;
        $("#ticket-list").hide();

    }
});

