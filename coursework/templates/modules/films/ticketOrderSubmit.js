$("#sub").click(function (ev) {
    ev.preventDefault();
    var selectedSeats = [];
    $("#selected-seats .choosed").each(function () {
        var tmp = {};
        tmp['seat'] = $('.seat-index',this).text();
        tmp['row'] = $('.row-index',this).text();
        selectedSeats.push(tmp);
    });
    $("#selected-seats").parent().find('button').prop('disabled', true);
    var json = JSON.stringify({data:selectedSeats});
    $.ajax({ url: '/films/ticketSelection',
        data: {data: json,sessionId:$("#sessionId").val()},
        type: 'POST',
        success: function(res) {
            console.log(res);

            if(res==1){
                var div = $("div.successNotif");

                //div.hide();
                //$("#result-info").append(div)
                div.fadeIn('slow');
                //$("#result-info").append(div);
            }
            else{
                var div = $("div.errorNotif");
                console.log(div);
                //div.hide();
                //$("#result-info").append(div);
                div.fadeIn('slow');
            }
            setTimeout(function() {
                $("#result-info div").fadeOut('slow');
                $("#selected-seats .choosed").fadeOut('slow');
                $("#ticket-list").fadeOut('slow');
                selectedSeats.forEach(function (item,i) {
                    var a = $('div.hall-row:nth-child('+(+item['row']+1)+') div.selected-seat:nth-child('+item['seat']+')');
                    a.removeClass('selected-seat');
                    a.addClass('sold');
                });
                setTimeout(function () {
                    $("#selected-seats .choosed").remove();
                }, 610);
            }, 6000);
        }
    });

});