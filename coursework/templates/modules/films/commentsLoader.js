var offset = 0;
var commentsN = 5;
var filmId;
var userImageId;
window.onload = function () {
    filmId = $("#filmid").val();

    var userLogin = $("#user-login").text();
    $("#getMoreComments").click(getComments);
    getComments();
    $.ajax({
        url: '/users/getUserImageId',
        data: {
            userLogin: userLogin
        },
        type: 'POST',
        success: function (res) {
            userImageId = '/files/users/'+res;
        }
    });
};

$("#sendComment").click(function (ev) {
    ev.preventDefault();
    var userLogin = $("#user-login").text();
    var d = new Date();
    var options = {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric'
    };
    var datetime = d.toLocaleString('uk', options).replace(',', '');
    var content = $("textarea").val();
    if (content == '')
        return;
//GetUserImageId

    var $comment = $("<div style='display: none' class=\"media mb-4\">\n" +
        "    <img class=\"d-flex mr-3 rounded-circle\" src='"+userImageId+"' alt=\"\">\n" +
        "    <div class=\"media-body\">\n" +
        "        <h5 class=\"mt-0\">" + userLogin + "</h5>\n" +
        "        <span class=\"time\">" + datetime + "</span>\n" +
        "        " + content + "\n" +
        "    </div>\n" +
        "</div>");

    $("#comment-container").prepend($comment);

    $comment.fadeIn('slow');
    $.ajax({
        url: '/comments/addComment',
        data: {
            id: filmId,
            userLogin: userLogin,
            datetime: datetime,
            content: content
        },
        type: 'POST',
        success: function (res) {
        }
    });
});

function getComments() {
    $.ajax({ url: '/comments/getCommentsToFilm',
        data: {
            id: filmId,
            offset: offset,
            commentsN: commentsN
        },
        type: 'POST',
        success: function(res) {
            var arr = JSON.parse(res);

            offset+= commentsN;
            for(var i=0;i<arr.length;i++){
                arr[i]['imageid'] = "/files/users/"+arr[i]['imageid'];
                if($('input#role').val()=='admin') {
                    var button = $("<button class='btn btn-dark btn-sm delete-comment' data-id='"+arr[i].id+"'>delete</a>");
                    button.click(deleteFun)
                    var $comment = $("<div style='display: none' class=\"media mb-4\">\n" +
                        "    <img class=\"d-flex mr-3 rounded-circle\" src=\"" + arr[i]['imageid'] + "\" alt=\"\">\n" +
                        "    <div class=\"media-body\">\n" +
                        "        <h5 class=\"mt-0\">" + arr[i]['login'] + "</h5>\n" +
                        "        <span class=\"time\">" + arr[i]['datetime'] + "</span>\n" +
                        "        " + arr[i]['content'] + "\n" +
                        "    </div>\n" +
                        //button +
                        "</div>");

                    $comment.append(button);
                }
                else

                    var $comment = $("<div style='display: none' class=\"media mb-4\">\n" +
                        "    <img class=\"d-flex mr-3 rounded-circle\" src=\""+arr[i]['imageid']+"\" alt=\"\">\n" +
                        "    <div class=\"media-body\">\n" +
                        "        <h5 class=\"mt-0\">"+arr[i]['login']+"</h5>\n" +
                        "        <span class=\"time\">"+arr[i]['datetime']+"</span>\n" +
                        "        "+arr[i]['content']+"\n" +
                        "    </div>\n" +
                        "</div>");
                $comment.insertBefore($("#getMoreComments"));

                $comment.fadeIn('slow');
            }

        }
    });
}

function deleteFun(ev) {
    //console.log($(this).attr('data-id') );
    var $c = $(this).parent();
    $.ajax({ url: '/comments/delete',
        data: {
            id: $(this).attr('data-id'),
        },
        type: 'post',
        success: function(res) {
            $c.fadeOut('slow');
    }});
}