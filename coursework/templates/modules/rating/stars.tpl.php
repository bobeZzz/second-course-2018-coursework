<div id="<?=$id?>">
    <?php
    $mark = intval($ratings['rating']['rating']);
    if(isset($ratings['userRating']))
        $usermark = intval($ratings['userRating']['mark']);
    else
        $usermark = 0;
    echo '<input class=filmmark type="hidden" value="'.$mark.'">';

    for($i = 0;$i < 5;$i++)
    {
        if($i< $mark)
            echo '<span class="fa fa-star fa-2x checked"></span>';
        else
            echo '<span class="fa fa-star fa-2x "></span>';
    }
    ?>
    <!--<span class="marks">Середня оцінка: <?=$mark?></span>-->
    <?php
    if (isset($_SESSION['login'])){
        echo '    <span class="marks">Ваша оцінка: '.$usermark.'</span>';
    }
    ?>
    <!--<span class="fa fa-star checked"></span>
    <span class="fa fa-star"></span>-->
</div>