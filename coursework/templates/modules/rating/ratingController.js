$("span.fa.fa-star").hover(function () {

    var indexOflast = $(this).index();
    $("span.fa.fa-star",$(this).parent()).each(function () {
        $(this).removeClass('checked');
        if($(this).index()<=indexOflast)
        {
            $(this).addClass('checked');
        }
    });
});

$("span.fa.fa-star").parent().mouseout(function () {

    $("span.fa.fa-star",$(this).parent()).each(function () {
        if($(this).index()<=$(".filmmark",$(this).parent()).val())
        {
            $(this).addClass('checked');
        }
        else{
            $(this).removeClass('checked');
        }

    });
});
var context;

$("span.fa.fa-star").click(function () {
    var mark = $(this).index() ;

    var userLogin = $("#user-login").text();
    var filmId = $(this).parent().attr('id');
    context = this;
    $.ajax({
        url: '/rating/setRating',
        data: {
            id: filmId,
            userLogin: userLogin,
            mark: mark
        },
        type: 'POST',
        success: function (res) {
            $.ajax({
                url: '/rating/getRating',
                data: {
                    id: filmId,
                    userLogin: userLogin
                },
                type: 'POST',
                success: function (res) {
                    var newavg = Math.round(JSON.parse( res)['rating']['rating']);
                    $(".filmmark",$(context).parent()).val(newavg);

                   
                    $("span:last-child",$(context).parent()).text('Ваша оцінка: '+JSON.parse( res)['userRating'][0]);
                    $("span.fa.fa-star",$(context).parent()).each(function () {
                        $(this).removeClass('checked');
                        if($(this).index()<=newavg)
                        {
                            $(this).addClass('checked');
                        }
                    });

                }
            });

        }
    });
});
/* $("span.fa.fa-star",$(this).parent()).each(function () {
                $(this).removeClass('checked');
                if($(this).index()<=)
                {
                    $(this).addClass('checked');
                }
            });*/