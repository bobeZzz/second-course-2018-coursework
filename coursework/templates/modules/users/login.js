$( "form" ).submit(function(e) {
    $("#loginin").prop( "disabled", true );
    e.preventDefault();
       $.ajax({ url: '/users/login',
            data: {
            login: $( "#login" ).val(),
            password: $( "#password" ).val()},
            type: 'post',
            success: function(output) {
                console.log(output);
                if(output=='1')
                {
                    var url = "/";
                    window.location.href = url;
                }
                else{
                    $("#loginin").prop( "disabled", false );
                    $( "#login" ).css('background-color','#D34831');
                    $( "#password" ).css('background-color','#D34831');
                }
            }
        });
    return;
});
