<div class="container">

        <div class="main-login main-center">
            <div class="">
                <form class="form-horizontal" method="post" action="/users/resetPassword">

                    <div class="form-group">
                        <label for="username" class="cols-sm-2 control-label">Email</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope  fa" aria-hidden="true"></i></span>
                                <input type="email" class="form-control" name="email" id="email"  placeholder="Enter your Email"/>
                            </div>
                        </div>
                    </div>


                    <div class="form-group ">
                        <button type="submit" class="btn btn-dark btn-lg btn-block login-button" id="loginin">Reset</button>
                    </div>
                    <div class="login-register">
                        <a href="/users/login">Login</a>
                        <br>
                        <a href="register">Register</a>
                    </div>
                </form>
        </div>
    </div>
</div>