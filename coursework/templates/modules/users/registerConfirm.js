var  vlogin,vemail,vpassword;
vlogin=vemail=vpassword=false;
$( "#login" ).focusout(function() {
    if($( "#login" ).val()=='')
    {
        $("#login").css('background-color','#D34831');
        return;
    }
    else
    $.ajax({ url: '/users/CheckLogin',
        data: {login: $( "#login" ).val()},
        type: 'post',
        success: function(output) {
            if(output!='0')
                $("#login").css('background-color','#D34831');
            else{
                $("#login").css('background-color','#52d354');
                vlogin = true;
            }
        }
    });
});

$( "#email" ).focusout(function() {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!regex.test($("#email").val())){
        $("#email").css('background-color','#D34831');
        return;
    }
    else
    $.ajax({ url: '/users/CheckEmail',
        data: {email: $( "#email" ).val()},
        type: 'post',
        success: function(output) {
            if(output!='0')
                $("#email").css('background-color','#D34831');
            else{
                $("#email").css('background-color','#52d354');
                vemail = true;
            }
        }
    });
});

$( "#confirm" ).focusout(function() {
    if($( "#confirm" ).val() == $( "#password" ).val() && $( "#password" ).val()!='')
        $("#confirm").css('background-color','#52d354');
    else{
        $("#confirm").css('background-color','#D34831');
        vpassword = true;
    }
});

$( "#password" ).focusout(function() {
    if($( "#password" ).val()=='')
        $("#password").css('background-color','#D34831');

});

$( "#login" ).focus(function () {
    $("#login").css('background-color','');
});
$( "#email" ).focus(function () {
    $("#email").css('background-color','');
});
$( "#confirm" ).focus(function () {
    $("#confirm").css('background-color','');
});
$( "#password" ).focus(function () {
    $("#password").css('background-color','');
});


$("#rgister").click(function (ev) {
    var valid = true;
    if($("#password").val()==''){
        $("#password").css('background-color','#D34831');
        valid = false;
    }

    if($("#confirm").val()==''){
        $("#confirm").css('background-color','#D34831');
        valid = false;
    }
    if($("#login").val()==''){
        $("#login").css('background-color','#D34831');
    }
    if($("#password").val() != $("#confirm").val() && $("#password").val()!=''){
       $("#confirm").css('background-color','#D34831');
        valid = false;
    }
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!regex.test($("#email").val()) || $("#email").val() == '') {
        $("#email").css('background-color', '#D34831');
        valid = false;
    }

    if (valid)
    {

    }
    else
    {
        ev.preventDefault();
        return;
    }
});