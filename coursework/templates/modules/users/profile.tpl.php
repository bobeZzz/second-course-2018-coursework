<div class="container">
    <div class="row">
        <div class="col-lg-2">

        </div>
        <div class=" col-lg-4">
            <h2>Welcome back <?=$_SESSION['login']?></h2><!-- Project One -->
            <h4>Avatar</h4>
            <?php
                if(isset($imageId) && $imageId!='')
                    echo '<img src="/files/users/'.$imageId.'" class="avatar">';
                else
                    echo '<img src="" class="avatar" style="display: none;">';
            ?>

            <form action="/users/updateAvatar" enctype="multipart/form-data"  data='avatar'>
                <input type="file" name="img">
                <br>
                <input type="submit" class="btn btn-dark" value="Update">
            </form>
        </div>
        <div class=" col-lg-4">
            <br>
            <br>
            <form action="/users/updatePassword" data='password'>
                <pre>old password:  <input type="password" name="old"></pre>
                <pre>new password:  <input type="password" name="newpassword"></pre>
                <input type="submit" class="btn btn-dark" value="Update">
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2">

        </div>
        <div class=" col-lg-8">
            <hr>
            <h3 >Film revision</h3>
            <br>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Film</th>
                    <th scope="col">Row</th>
                    <th scope="col">Seat</th>
                    <th scope="col">Date</th>
                    <th scope="col">Price</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 1;
                foreach ($films as $item) {

                    echo '<tr>';
                    echo '<th scope="row">'.$i.'</th>
                    <td><a href="/films/movie/'.$item['id'].'">'.$item['name'].'</a></td>
                    <td>'.$item['row'].'</td>
                    <td>'.$item['seat'].'</td>
                    <td>'.$item['date'].' '.$item['time'].'</td>
                    <td>'.$item['price'].'</td>';
                    echo '</tr>';
                    $i++;
                }
                ?>

                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $("form[data=avatar]").submit(function(ev){
        ev.preventDefault();
        var data = new FormData();
        jQuery.each(jQuery('input[type=\'file\']')[0].files, function(i, file) {
            data.append('file-'+i, file);
        });
        $.ajax({ url: '/users/updateAvatar',
            data: data,
            type: 'POST',
            cache: false,
            contentType: false,
            processData: false,
            success: function(res) {
                $('img.avatar').attr('src','/files/users/'+res);
                $('img.avatar').show();
                $('input[type=\'file\']').val('');
            }
        });
    });

    $("form[data=password]").submit(function(ev){
        ev.preventDefault();

        $.ajax({ url: '/users/updatePassword',
            data: {
                oldPass: $("input[name=old]").val(),
                newPass: $("input[name=newpassword]").val()
            },
            type: 'post',
            success: function(res) {
                console.log(res);
                if(res==0)
                    $("input[name=old]").css('background-color','#D34831');
                else{
                    $("input[name=old]").css('background-color','#52d354');
                    $("input[name=newpassword]").css('background-color','#52d354');

                }

            }
        });
    });

    $("input[name=old]").focus(function () {
       $(this).css('background-color','');;
    });
    $("input[name=newpassword]").focus(function () {
        $(this).css('background-color','');;
    });
</script>