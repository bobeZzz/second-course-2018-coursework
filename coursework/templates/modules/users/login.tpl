<div class="container">

        <div class="main-login main-center">
            <?php
        if(isset($regComplete)){
            if($regComplete==1)
                include ('templates/modules/users/registrationComplete.tpl');
            if($regComplete==2)
                include ('templates/modules/users/resetComplete.tpl');}
        ?>
            <div class="">
                <form class="form-horizontal" method="post" action="./login">

                    <div class="form-group">
                        <label for="username" class="cols-sm-2 control-label">Username</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="login" id="login"  placeholder="Enter your Username"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password" class="cols-sm-2 control-label">Password</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                <input type="password" class="form-control" name="password" id="password"  placeholder="Enter your Password"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group ">
                        <button type="submit" class="btn btn-dark btn-lg btn-block login-button" id="loginin">Login</button>
                    </div>
                    <div class="login-register">
                        <a href="/users/resetPassword">Forgot password?</a>
                        <br>
                        <a href="register">Register</a>
                    </div>
                </form>
                <script src="/templates/modules/users/login.js"></script>
        </div>
    </div>
</div>