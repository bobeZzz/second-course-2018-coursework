<?php
class Comments_Controller
{
    public function AddCommentAction()
    {
        Core::$IndexTpl->output = false;
        $res = Comments_Model::addComment($_POST['id'],$_POST['userLogin'],$_POST['datetime'],$_POST['content']);
        echo json_encode($res);;
    }

    public function GetCommentsToFilmAction()
    {
        Core::$IndexTpl->output = false;
                                                           //id/limit/offset
        $comments = Comments_Model::getComments($_POST['id'],$_POST['commentsN'],$_POST['offset']);
        //var_dump($comments);
        $res = json_encode($comments);
        echo $res;
        // $_POST['id'];
        // $_POST['offset'];
    }

    public function DeleteAction()
    {
        Core::$IndexTpl->output = false;
        Comments_Model::delete($_POST['id']);

    }

}