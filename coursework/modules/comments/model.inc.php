<?php
class Comments_Model
{
    public static function getComments($id,$limit,$offset)
    {
        $db = new DB(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        $res = $db->GetComments($id,$limit,$offset);

        return  $res;
    }

    public static function addComment($filmid, $userlogin, $datetime,$content)
    {
        $db = new DB(DB_HOST,DB_USER,DB_PASS,DB_NAME);

        $datetime = explode(' ',$datetime);

        $date = explode('.',$datetime[0]);
        $datetime = $date[2].'-'.$date[1].'-'.$date[0].' '.$datetime[1];//+

        $res = $db->AddComment($filmid,$userlogin,$datetime,$content);

        return  $res;
    }

    public static function delete($id)
    {
        $db = new DB(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        $res = $db->deleteCommnet($id);

        return  $res;
    }
}