<?php
class Films_Controller
{
    public function MovieAction($params)
    {
        $movieId = $params[0];
        //get from db
        $res = Films_Model::getFilmById($movieId);
        if($movieId==0 || !isset($movieId) || $res==null)
        {
            $controller = new Main_Controller();
            $res = $controller->Error404Action();
            return;
        }
        //get comments to film
        //$comments = Films_Model::getCommentsByFilmId($movieId);
        //get comments to film

        Films_View::filmFullDescriptionView($res[0]);

    }

    public function GetFilmByIdAction()
    {
        Core::$IndexTpl->output= false;
        $res = Films_Model::getFilmFullInfo($_POST['id'])[0];
        echo json_encode($res);
    }


    public function UpdateAction()
    {
        //$_POST $_FILES
        header('Location: /films/editFilm');
        Films_Model::UpdateFilm($_POST,$_FILES);
    }

    public function DeleteAction()
    {
        //$_POST $_FILES
        Core::$IndexTpl->output= false;
        Films_Model::DeleteFilm($_POST['id']);
    }


    public function TicketSelectionAction($params)
    {
        if (!isset($_SESSION['login'])){
            header('Location: /users/login');
            return;
        }
        switch ($_SERVER['REQUEST_METHOD']) {
            case 'GET':
                if(!isset($params[0]))
                {
                    $controller = new Main_Controller();
                    $res = $controller->Error404Action();
                    return;
                }
                $viewParams = [];
                $seats = Films_Model::getTicketsBySessionId($params[0]);
                $viewParams['seats'] = $seats ;
                $movieInfo = Films_Model::getFilmInfoBySessionId($params[0]);
                $viewParams['info'] = $movieInfo[0];
                $viewParams['info']['sessionId'] = $params[0];

                Films_View::ticketSelection($viewParams);
                break;
            case 'POST':
                Core::$IndexTpl->output = false;

                $seats = json_decode(stripslashes($_POST['data']))->data;
                $sessionId = $_POST['sessionId'];
                $t = Users_Model::AddToHistory($seats,$_SESSION['login'],$sessionId);
                $res = Films_Model::orderSeats($seats,$sessionId);

                echo $res;

                break;
            }
    }

    public function AddAction()
    {
        $res = $this->isModerator();
        if(!$res)
            return;

        switch ($_SERVER['REQUEST_METHOD']) {
            case 'GET':
                $tpl = 'templates/modules/films/addNewMovie.tpl';
                Core::$IndexTpl->setContentView($tpl);
                break;
            case 'POST':
                header('Location: /films/add');

                $res = Films_Model::addFilm($_POST,$_FILES);

                break;
        }
    }

    public function AddSessionAction()
    {
        $res = $this->isModerator();
        if(!$res)
            return;

        switch ($_SERVER['REQUEST_METHOD']) {
            case 'GET':
                $tpl = 'templates/modules/films/addSession.tpl.php';
                // get all films id + names
                $allFilms = Films_Model::getAllFilms();
                Core::$IndexTpl->setContentView($tpl);
                Core::$IndexTpl->setParam('films',$allFilms);
                break;
            case 'POST':
                //
                Films_Model::addSession($_POST);
                //

                $controller = new Main_Controller();
                $res = $controller->IndexAction();
                header('Location: /');
                break;
        }
    }

    public function EditFilmAction()
    {
        $films = Films_Model::getAllFilms();

        Core::$IndexTpl->setContentView('templates/modules/films/editMovie.tpl.php');

        Core::$IndexTpl->setParam('films',$films);

    }

    public function EditBookAction()
    {
        $films = Films_Model::getAllFilms();
        var_dump($films);die();
        Core::$IndexTpl->setParam('films',$films);

        Core::$IndexTpl->setContentView('templates/modules/films/editMovie.tpl.php');
    }


    public function isModerator()
    {
        $haveRules = false;
        if(isset($_SESSION['role']))
            if( $_SESSION['role']=='admin' || $_SESSION['role']=='moderator')
                $haveRules = true;

        if(!$haveRules)
        {
            $controller = new Main_Controller();
            $res = $controller->Error404Action();
        }

        return $haveRules;
    }

    public function DeleteSessionAction()
    {
        Core::$IndexTpl->output= false;
        $res = Films_Model::deleteSessionById($_POST['sessionid']);
        //echo $res;
        var_dump($res);
    }
}