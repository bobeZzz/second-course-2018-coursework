<?php
class Films_View
{
    public static function filmFullDescriptionView($filmInfo)
    {
        $tpl = Core::$IndexTpl;
        $tpl->setContentView('templates/modules/films/filmFullDescription.tpl.php');
        $tpl->setParams($filmInfo);
    }

    public static function ticketSelection($filmInfo)
    {
        $tpl = Core::$IndexTpl;
        $tpl->setContentView('templates/modules/films/ticketSelection.tpl.php');
        $tpl->setParams($filmInfo);
    }


}