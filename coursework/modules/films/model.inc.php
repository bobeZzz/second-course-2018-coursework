<?php
class Films_Model
{
    public static function addFilm($arr,$files)
    {
        $poster = $files['poster'];
        $arr['imageId'] = uniqid().'.'.explode('/',$poster['type'])[1];
        $target = 'files/films/'.$arr['imageId'];
        move_uploaded_file($poster['tmp_name'],$target);

        //save image somewhere with name $row['imageId']

        $db = new DB(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        $res = $db->AddFilm($arr);

        return  $res;
    }

    public static function getFilmById($id)
    {
        $db = new DB(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        $res = $db->GetFilmById($id);
        // write session into res[0];
        $temp = $db->GetSessionsByMovieId($id);
        $res[0]['sessions']  = array();
        foreach ($temp as $item){

            $tempArr = array("time"=> $item['time'],'id'=>$item['id']);
            if(!isset($res[0]['sessions'][$item['date']]))
                $res[0]['sessions'][$item['date']] = array();
            array_push($res[0]['sessions'][$item['date']],$tempArr);

        }
        $ul = 0;
        if(isset($_SESSION['login']))
            $ul = $_SESSION['login'];
        $res[0]['ratings'] = Rating_Model::getRating($id,$ul);
        return  $res;
    }

    public static function getAllFilms()
    {
        $db = new DB(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        $res = $db->GetAllFilms();

        return  $res;
    }


    public static function addSession($arr)
    {
        $db = new DB(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        $res = $db->AddSession($arr);

        return  $res;
    }

    public static function getFilmInfoBySessionId($sessionId)
    {
        $db = new DB(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        $res = $db->GetFilmInfoBySessionId($sessionId);

        return  $res;
    }

    public static function getTicketsBySessionId($sessionId)
    {
        $db = new DB(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        $res = $db->GetTicketsBySessionId($sessionId);
        $returnArr = [];
        $counter = 0;
        for($i = 0;$i<7;$i++){
            $returnArr[$i] = [];
            for($k = 0;$k<12;$k++){
                $returnArr[$i][$k] = $res[$counter];
                $counter++;
            }
        }
        return  $returnArr;
    }

    public static function orderSeats($seatsObjArr,$sessionId)
    {
        $db = new DB(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        $res = $db->CheckIfSeatsIsNotSold($seatsObjArr,$sessionId);

        if($res!=1)
            return -1;
        else{
            $res = $db->OrderSeats($seatsObjArr,$sessionId);
        }
        return  $res;
    }

    public static function DeleteSessionById($id)
    {
        $db = new DB(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        $res = $db->DeleteSessionById($id);

        return  $res;
    }

    public static function getFilmFullInfo($id)
    {
        $db = new DB(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        $res = $db->GetFilmFullInfo($id);

        return  $res;
    }

    public static function UpdateFilm($row,$files)
    {
        $db = new DB(DB_HOST,DB_USER,DB_PASS,DB_NAME);

        var_dump($row['imageId']);
        if($files['poster']['error']==0)
        {
            $poster = $files['poster'];
            $row['imageId'] = uniqid().'.'.explode('/',$poster['type'])[1];
            $target = 'files/films/'.$row['imageId'];
            move_uploaded_file($poster['tmp_name'],$target);
        }
        var_dump($row['imageId']);

        $res = $db->UpdateFilm($row);

        return  $res;
    }

    public static function DeleteFilm($id)
    {
        $db = new DB(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        $res = $db->DeleteFilm($id);

        return  $res;
    }

}