<?php
class Users_View
{
    public static function Profile()
    {
        $tpl = Core::$IndexTpl;
        $tpl->setContentView('templates/modules/users/profile.tpl.php');
    }

    public static function ResetPassword()
    {
        $tpl = Core::$IndexTpl;
        $tpl->setContentView('templates/modules/users/resetPassword.tpl');
    }
}