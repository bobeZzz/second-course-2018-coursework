<?php
class Users_Model
{
    public static function ActivateAccount($token)
    {
        $db = new DB(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        $db->ActivateAccount($token);
        header("Location: /users/login");
    }

    public static function sendMail($email,$theme,$message)
    {
        $headers = "Content-Type: text/html; charset=ISO-8859-1\r\n";

        mail($email,$theme,$message,$headers);
    }

    public static function ResetPassword($email)
    {
        $db = new DB(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        $password = uniqid();
        $db->ResetPassword($email,$password);

        $message = '<a href="http://' . $_SERVER["HTTP_HOST"] . '/users/login/' . $row['token'] . '">Click here to login<a/> ';
        $message .= "<br/><br/>Your new password is ".$password;
        $message = wordwrap($message, 70, "\r\n");

        self::sendMail($email,'Account recovery', $message);
    }

    public static function changeUserStatus($userId,$newStatus)
    {

    }

    public static function registerUser($row)
    {
        $row['token'] = md5(uniqid());
        // send email

        $message = '<a href="http://' . $_SERVER["HTTP_HOST"] . '/users/activate/' . $row['token'] . '">Click here to activate your account<a/> ';
        $message .= "<br/><br/>If you recieved this message by accident just ignore it.";
        $message = wordwrap($message, 70, "\r\n");


        self::sendMail($row['email'],'Account activation', $message);
        // send email
        $db = new DB(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        $db->AddNewUser($row);
    }

    public static function getUserPasswordByLogin($login,$password)
    {
        $db = new DB(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        $res = $db->getUserPasswordByLogin($login);
        if($res != -1 && $res['password']==md5($password)){
            $_SESSION['login']=$res['login'];
            $_SESSION['role']=$res['role'];
            return true;
        }
        else
            return false;
    }

    public static function GetUsersFilms($login)
    {
        $db = new DB(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        $res = $db->GetUsersFilms($login);
        return $res;
    }

    public static function AddToHistory($seats,$login,$sessionId)
    {
        $db = new DB(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        $res = $db->AddToHistory($seats,$login,$sessionId);
        return $res;
    }

    public static function GetUserImageId($login)
    {
        $db = new DB(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        $res = $db->GetUserImageId($login);
        return $res;
    }

    public static function UpdateAvatar($files)
    {
        $poster = $files['file-0'];
        $imageId = uniqid().'.'.explode('/',$poster['type'])[1];
        $target = 'files/users/'.$imageId;
        move_uploaded_file($poster['tmp_name'],$target);

        //save image somewhere with name $row['imageId']

        $db = new DB(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        $res = $db->UpdateAvatar($_SESSION['login'],$imageId);

        return  $imageId;
    }

    public static function UpdatePassword($old,$new)
    {

        $db = new DB(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        $res = $db->UpdatePassword($_SESSION['login'],$old,$new);

        return  $res;
    }


}