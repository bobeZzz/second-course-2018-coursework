<?php
class Users_Controller
{


    public function RegisterAction()
    {
        if (isset($_SESSION['login'])){
            header('Location: /');
            return;
        }
        switch ($_SERVER['REQUEST_METHOD'])
        {
            case 'GET':
                    $tpl = 'templates/modules/users/register.tpl';
                    Core::$IndexTpl->setContentView($tpl);
                break;
            case 'POST':
                Users_Model::registerUser($_POST);

                header("Location: /users/login/1");

        }

    }

    public function ActivateAction($params)
    {
        $token = array_shift($params);

        Users_Model::ActivateAccount($token);
    }

    public function ResetPasswordAction()
    {
        if (isset($_SESSION['login'])){
            header('Location: /');
            return;
        }
        switch ($_SERVER['REQUEST_METHOD']) {
            case 'GET':
                Users_View::ResetPassword();
                break;
            case 'POST':
                header('Location: /users/login/2');
                Users_Model::ResetPassword($_POST['email']);
                break;
        }


    }

    public function LoginAction($params)
    {
        if (isset($_SESSION['login'])){
            header('Location: /');
            return;
        }
        switch ($_SERVER['REQUEST_METHOD'])
        {
            case 'GET':
                $tpl = 'templates/modules/users/login.tpl';
                Core::$IndexTpl->setContentView($tpl);
                if(isset($params[0]))
                Core::$IndexTpl->setParam('regComplete',$params[0]);
                break;
            case 'POST':
                $res = Users_Model::getUserPasswordByLogin($_POST['login'],$_POST['password']);
                Core::$IndexTpl->output= false;
                if($res)
                    echo true;
                else
                    echo false;
        }
    }

    public function LogoutAction()
    {
        session_unset();
        session_destroy();
        header('Location: /');
    }

    public function CheckLoginAction()
    {
        $db = new DB(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        $res = $db->IsLoginUsed($_POST['login']);
        Core::$IndexTpl->output= false;

        echo $res;
    }

    public function CheckEmailAction()
    {
        $db = new DB(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        $res = $db->IsEmailUsed($_POST['email']);
        Core::$IndexTpl->output= false;

        echo $res;
    }

    public function ProfileAction()
    {
        if(!isset($_SESSION['login'])){
            $controller = new Main_Controller();
            $res = $controller->Error404Action();
            return;
        }

        $films = Users_Model::GetUsersFilms($_SESSION['login']);
        $imageId = Users_Model::GetUserImageId($_SESSION['login'])[0][0];

        Core::$IndexTpl->setParam('films',$films);
        Core::$IndexTpl->setParam('imageId',$imageId);
        Users_View::Profile();

    }

    public function UpdateAvatarAction()
    {
        Core::$IndexTpl->output= false;
        $imgId = Users_Model::UpdateAvatar($_FILES);
        echo $imgId;
    }

    public function UpdatePasswordAction()
    {
        Core::$IndexTpl->output= false;
        $res = Users_Model::UpdatePassword($_POST['oldPass'],$_POST['newPass']);
        echo $res;
    }

    public function GetUserImageIdAction()
    {

        Core::$IndexTpl->output = false;
        $res = Users_Model::GetUserImageId($_POST['userLogin'])[0][0];
        echo $res;
    }

}