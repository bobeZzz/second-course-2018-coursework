<?php
class Main_View
{
    public static function indexView($films)
    {
        $tpl = Core::$IndexTpl;
        $tpl->setContentView('templates/index.tpl.php');
        $tpl->setParam('arr',$films);

    }
}