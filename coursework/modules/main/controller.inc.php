<?php
class Main_Controller
{
    public function IndexAction()
    {


        $arr = Main_Model::getCurrentMoviesPreview();
        Main_View::indexView($arr);

    }

    public function Error404Action()
    {
        Core::$IndexTpl->setContentView('templates/error404.tpl');
    }

}