<?php
class Rating_Model
{
    public static function getRating($id,$userlogin)
    {
        $db = new DB(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        $res = $db->GetRating($id,$userlogin);

        return  $res;
    }

    public static function setRating($filmid, $userlogin, $rating)
    {
        $db = new DB(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        $res = $db->SetRating($filmid,$userlogin, $rating);

        return  $res;
    }
}