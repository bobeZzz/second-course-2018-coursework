<?php
class Rating_Controller
{
    public function SetRatingAction()
    {
        Core::$IndexTpl->output = false;
        $res = Rating_Model::setRating($_POST['id'],$_POST['userLogin'],$_POST['mark']);
        echo json_encode($res);
    }

    public function GetRatingAction()
    {
        Core::$IndexTpl->output = false;
        //id/limit/offset
        $comments = Rating_Model::getRating($_POST['id'],$_POST['userLogin']);
        //var_dump($comments);
        $res = json_encode($comments);
        echo $res;
        // $_POST['id'];
        // $_POST['offset'];
    }
}